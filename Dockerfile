FROM jsurf/rpi-raspbian:latest

ARG PARAM_AGENT_VERSION
ARG PARAM_DOCKER_VERSION

ENV DOCKERVERSION=${PARAM_DOCKER_VERSION}
ENV VERSION=${PARAM_AGENT_VERSION}

ARG user=jenkins
ARG group=jenkins
ARG uid=1000
ARG gid=1000

RUN groupadd -g ${gid} ${group}
RUN useradd -c "Jenkins user" -d /home/${user} -u ${uid} -g ${gid} -m ${user}
LABEL Description="This is a base image, which provides the Jenkins agent executable (agent.jar)" Vendor="Jenkins project" Version="${VERSION}"

ARG AGENT_WORKDIR=/home/${user}/agent

RUN apt-get update && apt-get install git openjdk-8-jdk ca-certificates && rm -rf /var/lib/apt/lists/*
RUN curl --create-dirs -k https://repo.jenkins-ci.org/public/org/jenkins-ci/main/remoting/${VERSION}/remoting-${VERSION}.jar -o /usr/share/jenkins/agent.jar \
  && chmod 755 /usr/share/jenkins \
  && chmod 644 /usr/share/jenkins/agent.jar \
  && ln -sf /usr/share/jenkins/agent.jar /usr/share/jenkins/slave.jar

RUN curl -k https://download.docker.com/linux/static/stable/armhf/docker-${DOCKERVERSION}.tgz -o docker-${DOCKERVERSION}.tgz \
  && tar xzvf docker-${DOCKERVERSION}.tgz \
  && mv docker/docker /usr/bin/ \
  && rm docker-${DOCKERVERSION}.tgz \
  && rm -rf docker

ENV AGENT_WORKDIR=${AGENT_WORKDIR}
RUN mkdir /home/${user}/.jenkins && mkdir -p ${AGENT_WORKDIR}

VOLUME /home/${user}/.jenkins
VOLUME ${AGENT_WORKDIR}
WORKDIR /home/${user}
